package jp.alhinc.yoshizawa.satoru.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] args) {
		BufferedReader br = null;
		BufferedReader sd =null;
		Map<String, String> codeName = new HashMap<>();
		Map<String,Long> branhSales = new HashMap<String,Long>();
		ArrayList<File> dataName = new ArrayList<File>();



		try {
		   File file = new File (args[0],"branch.lst");

		   //1文字ずつで読み込む
		   FileReader fr = new FileReader (file);
		   //行で読み込む
		   br = new BufferedReader (fr);

		   String line;


		   while((line = br.readLine()) != null) {

			    String[] lines = line.split(",",0);;

			    String code = lines[0];
				String branch = lines[1];

				codeName.put(code,branch);

				branhSales.put(code,0L);
				if(!lines[0].matches("^[0-9]{3}-[,]-[^0-9]$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					br.close();
				   }
		   }

		}
		catch (FileNotFoundException nf){
			System.out.println("支店定義ファイルが存在しません");
		}
		catch(IOException errorLog){
			System.out.println("エラーが発生しました。");
		}
		  finally {
			  if(br != null) {
				  try {
					br.close();
				  } catch(IOException errorLog) {
						System.out.println("ファイルを閉じれませんでした。");
				  	}
			  }
		  }



		//ファイル検索、売り上げfileだけ加える
		File sale = new File(args[0]);
		File[] sales = sale.listFiles();
		for(int i=0; i< sales.length; ++i){
			String name = sales[i].getName();
			if(name.matches("[0-9]{8}.rcd")) {
				dataName.add(sales[i]);
			}
		}
		//売り上げファイルを読み込む
		try {
			for(int i = 0; i < dataName.size(); ++i) {

				File da = new File(args[0],dataName.get(i).getName());
				FileReader salesData = new FileReader(da);
				sd = new BufferedReader (salesData);


				String salesContents;

				ArrayList<String> vline = new ArrayList<String>();

				while((salesContents = sd.readLine()) != null) {
					vline.add(salesContents); // 売り上げファイル（読み込み済）をリストに加えた
				}
				long sContents = branhSales.get(vline.get(0));// mapから値を取り出す

				long salesDay = Long.parseLong(vline.get(1));// リストの売り上げ金額をLong型にキャストする
				long saled = sContents + salesDay ; // mapから取り出した値とキャストした値を足す
				// 足した値をmapにしまう
				branhSales.put(vline.get(0),saled);
			}

		} catch(IOException errorLog){
			System.out.println("エラーが発生しました。");

		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException errorLog) {
					System.out.println("ファイルを閉じれませんでした。");

				}
			}







			try {
				File salesfile = new File(args[0],"branch.out");
				FileWriter writerFile = new FileWriter(salesfile);
				BufferedWriter bw = new BufferedWriter(writerFile);


				for(Map.Entry<String,String> entry : codeName.entrySet()){

					bw.write(entry.getKey() + " , " + entry.getValue() + " , "  + branhSales.get(entry.getKey()));
					bw.newLine();

				}
				bw.close();
				}






			 catch(IOException e) {

			}
	}
	}

}
